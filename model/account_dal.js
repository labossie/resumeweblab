var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT * FROM account WHERE account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (account_name) VALUES (?)';

    var queryData = [params.account_name];

    connection.query(query, params.account_name, function(err, result) {

        // THEN USE THE account_id RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO account_address
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_address (account_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountAddressData = [];
        if (params.address_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                accountAddressData.push([account_id, params.address_id[i]]);
            }
        }
        else {
            accountAddressData.push([account_id, params.address_id]);
        }

        // NOTE THE EXTRA [] AROUND accountAddressData
        connection.query(query, [accountAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var accountAddressInsert = function(account_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_address (account_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            accountAddressData.push([account_id, addressIdArray[i]]);
        }
    }
    else {
        accountAddressData.push([account_id, addressIdArray]);
    }
    connection.query(query, [accountAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountAddressInsert = accountAddressInsert;

//declare the function so it can be used locally
var accountAddressDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_address WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountAddressDeleteAll = accountAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET account_name = ? WHERE account_id = ?';
    var queryData = [params.account_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete account_address entries for this account
        accountAddressDeleteAll(params.account_id, function(err, result){

            if(params.address_id != null) {
                //insert account_address ids
                accountAddressInsert(params.account_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};